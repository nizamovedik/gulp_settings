var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var less = require('gulp-less');
var path = require('path');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('scripts', function() {
  return gulp.src('alljs/*.js')
    .pipe(concat('scripts.js'))
    .pipe(uglify())
    .pipe(rename("script.min.js"))
    .pipe(gulp.dest('js/'));
});

gulp.task('default', function () {
  return gulp.src([
            path.join('scss/**/*.css'),
            path.join('scss/**/*.less'),
            ])
    .pipe(less())
    .pipe(concatCss("style.css"))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename("style.min.css"))
    .pipe(gulp.dest('css/'));
});

gulp.task('newtask', function() {
    console.log('ok');
});